import json
from input import input_data
from collections import defaultdict



class Solution:
    def __init__(self, data):
        self.sorted_data = data
        self.managers = defaultdict(list)
        self.watchers = defaultdict(list)

    def get_managers(self):
        for _ in self.sorted_data:
            for manager in _['managers']:
                self.managers[manager].append(_['name'])
        return dict(self.managers)

    def get_watchers(self):
        for _ in self.sorted_data:
            for watcher in _['watchers']:
                self.watchers[watcher].append(_['name'])
        return dict(self.watchers)

    def write_to_file(self, output, file):
        with open(file, 'w') as writer:
            writer.write(json.dumps(output, indent = 4))
        print(f"{file} created.")


if __name__ == '__main__':
    sorted_data = sorted(input_data, key=lambda x: x['priority'])
    solution = Solution(sorted_data)
    managers = solution.get_managers()
    watchers = solution.get_watchers()
    solution.write_to_file(dict(managers), 'managers.json')
    solution.write_to_file(dict(watchers), 'watchers.json')